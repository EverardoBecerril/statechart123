defmodule BulbWeb.PageLive do
  use BulbWeb, :live_view

  def mount(_params, _session, socket) do
    BulbWeb.Statechart.start_link(self())
    {:ok, assign(socket, lamp_is_on: false)}
  end

  def handle_event("switch", _, socket) do
    :gproc.send({:p, :l, :ui_event}, :switch)
    {:noreply, socket}
  end

  def handle_event("alarm", _, socket) do
    :gproc.send({:p, :l, :ui_event}, :alarm)
    {:noreply, socket}
  end

  def handle_cast(:switch_on, socket) do
    {:noreply, assign(socket, lamp_is_on: true)}
  end

  def handle_cast(:switch_off, socket) do
    {:noreply, assign(socket, lamp_is_on: false)}
  end
end